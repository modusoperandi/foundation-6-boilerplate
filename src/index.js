import './scss/index.css';
import './landing';
import './selection';
import $ from 'jquery/src/jquery';
import registerServiceWorker from './registerServiceWorker';

// load third party/vendor scripts
import 'what-input'
import 'foundation-sites/dist/js/foundation.min';

$(document).foundation();
registerServiceWorker();
